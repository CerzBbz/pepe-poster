from _operator import is_
import praw, random, time, re, io, csv

pattern = re.compile(
    "((\W(r|R)|^[R|r])([r|R]*)([E|e]{4,})(\W|$))|((\W(p|P)|(^[P|p]))(e|E)(p|P)(e|E)(\W|$|((S|s)"
    + "(\W|$))))|((r|R)(A|a)(R|r)(E|e)\W(P|p)(E|e)(P|p)(E|e))")

def main():
    with open('subreddits.csv', 'r') as file:
        reader = csv.reader(file)
        blocked_reddits = list(reader)
    r = praw.Reddit(user_agent="PepePoster by /u/Rare_Pepe_Poster")
    r.login(username, password)

    while True:
        print("Got new comments!")
        try:
            with open('blockedusers.csv', 'r') as file:
                reader = csv.reader(file)
                blocked_users = list(reader)
            with open('repliedcomment.csv', 'r') as file:
                reader = csv.reader(file)
                replied = list(reader)
            with open('repliedthread.csv', 'r') as file:
                reader = csv.reader(file)
                threads = list(reader)
            target = r.get_subreddit("test")
            comments = target.get_comments(limit=1000)
            for comment in comments:
                comment_text = comment.body.lower()
                found = pattern.search(comment_text)
                if found:  # if it matches our regex
                    print("-----------------STAGE 1-----------------")
                    print("Comment Matched")
                    subreddit = comment.subreddit
                    subreddit = str(subreddit).lower()
                    if subreddit not in (sr.lower() for sr in
                                         blocked_reddits[0]):  # if it's not bot-hating
                        print("-----------------STAGE 2-----------------")
                        print("Not banned here - " + subreddit)
                        printList(blocked_reddits[0])
                        thread = comment.submission
                        thread = re.sub(r"\d*\s::\s", "", str(thread))
                        thread = thread.lower()
                        if thread.lower() not in (t.lower() for t in threads[0]):  # if we haven't been here
                            print("-----------------STAGE 3-----------------")
                            print("Not posted here yet - " + thread)
                            printList(threads[0])
                            if str(comment.id) not in replied[0]:  # if we haven't talked to this guy
                                print("-----------------STAGE 4-----------------")
                                print("Haven't replied to this comment - " + str(comment.id))
                                printList(replied[0])
                                author = comment.author
                                author = str(author).lower()
                                if author not in (u.lower() for u in
                                                  blocked_users[0]):  # if they haven't asked us not to
                                    print("-----------------STAGE 5-----------------")
                                    print("Author: " + author + " hasn't asked us not to")
                                    printList(blocked_users[0])
                                    print("Full match found! Comment ID: " + comment.id)
                                    handle_ratelimit(comment.reply,
                                                     "**Potentially NSFW**\n\n[Here\'s a rare pepe, just for you!]"
                                                     + "(http://rarepe.pe/?i=" + str(random.randint(0, 2700))
                                                     + ")\n\n    I am a stupid bot, please don\'t hurt me.")
                                    print("Adding to blocked list")
                                    #Add it even if it fails to prevent future error targeting
                                    with open("repliedthread.csv", "a") as file:
                                        file.write(
                                            thread + ",")  # Adding it a list of replied threads
                                    with open("repliedcomment.csv", "a") as file:
                                        file.write(
                                            str(comment.id) + ",")  # Adding comment id to replied comments
                                    print("\______________________________________/")
                                else:
                                    print("\______________________________________/")
                            else:
                                print("\______________________________________/")
                        else:
                            print("\______________________________________/")
                    else:
                        print("\______________________________________/")
        except Exception as e:
            time.sleep(10)
            print(repr(e))
            continue


def handle_ratelimit(func, *args):
    while True:
        try:
            func(args)
            print("Reply succesful! See you in 10 minutes!")
            time.sleep(600)
            break
        except praw.errors.RateLimitExceeded as error:
            print('\tSleeping for %d seconds' % error.sleep_time)
            time.sleep(error.sleep_time)
        except praw.errors.Forbidden:
            print("Reply failed")
            break
        except Exception as e:
            print(repr(e))
            break

def printList(list):
    try:
        print(list)
    except Exception as e:
        print("Shit fucked up")

if __name__ == '__main__':
    main()